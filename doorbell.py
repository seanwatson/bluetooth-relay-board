import Queue
import bluetooth_connection
import sys
import logging
import string
import threading
import time
import xmlrpclib
from SimpleXMLRPCServer import SimpleXMLRPCServer

in_q = Queue.Queue()
out_q = Queue.Queue()

def switch():
    out_q.put('c')
    return 'ok'

class BluetoothPrinter(threading.Thread):
    
    def __init__(self, in_queue):
        threading.Thread.__init__(self)
        self.in_queue = in_queue
        self.kill = False
        self.timeout = 1

    def run(self):
        rec = ""
        while not self.kill:
            rec += str(self.in_queue.get(True))
            if len(rec) > 1:
                sys.stdout.write("%s\r\n" % (str(rec)))
                sys.stdout.flush()
                rec = ""

    def stop(self):
        self.kill = True
        self.in_queue.put(0)

def main():
    
    logging.basicConfig(level=logging.INFO)

    server = SimpleXMLRPCServer(("0.0.0.0", 6969))
    print "Listening on port 6969..."
    server.register_function(switch, "switch")

    # Bluetooth connection parameters
    dev_name = "DingALingADingDong"
    port = 1

    printer = BluetoothPrinter(in_q)
    btconn = bluetooth_connection.BluetoothConnection(dev_name, in_q, out_q, False)

    try:

        printer.start()
        btconn.start()

        while(btconn.connected == False):
            time.sleep(1)

        server.serve_forever()

    finally:
        btconn.stop()
        printer.stop()

        printer.join()
        btconn.join()
    

if __name__ == '__main__':
    main()
