Partlist

Exported from xbee_dongle.sch at 4/10/16 2:16 PM

EAGLE Version 7.3.0 Copyright (c) 1988-2015 CadSoft

Assembly variant: 

Part     Value                   Device                  Package           Library     Sheet

BT       BLUETOOTH-RN41          BLUETOOTH-RN41"         RN41              SparkFun-RF 1
C1       1uF                     CAP0603-CAP             0603-CAP          SparkFun    1
C2       10uF                    CAP_POL1206             EIA3216           SparkFun    1
C3       22p                     CAP0603-CAP             0603-CAP          SparkFun    1
C4       22p                     CAP0603-CAP             0603-CAP          SparkFun    1
C5       22uF                    CAP0805                 0805              CH_passive  1
C6       1u                      CAP0603-CAP             0603-CAP          SparkFun    1
C7       10uF                    CAP_POL1206             EIA3216           SparkFun    1
C8       0.1uF                   CAP0603-CAP             0603-CAP          SparkFun    1
C9       0.1uF                   CAP0603-CAP             0603-CAP          SparkFun    1
C11      10uF                    CAP1206                 1206              CH_passive  1
D1       1N4148                  DIODE1N4148             DIODE-1N4148      SparkFun    1
D2       Green                   LED0603                 LED-0603          SparkFun    1
D3       Red                     LED0603                 LED-0603          SparkFun    1
J1       CON_HEADER_PRG_AVR_ICSP CON_HEADER_PRG_AVR_ICSP HEADER_PRG_2X03   dp_devices  1
JP1                              JUMPER-2PTH             1X02              SparkFun    1
JP2                              JUMPER-2PTH             1X02              SparkFun    1
L2       4.7uH, 1.2A             INDUCTOR.               CDRH2D09          SparkFun    1
LED1                             LED0603                 LED-0603          SparkFun    1
LED2                             LED0603                 LED-0603          SparkFun    1
LED3                             LED0603                 LED-0603          SparkFun    1
LED4                             LED0603                 LED-0603          SparkFun    1
LED6                             LED0603                 LED-0603          SparkFun    1
MCU                              ATMEGA32U4-AU           TQFP44            adafruit    1
Q2       2N3904                  TRANSISTOR_NPNTO92      TO-92             SparkFun    1
Q3       MOSFET-NCHANNELSMD      MOSFET-NCHANNELSMD      SOT23-3           SparkFun    1
R1       22                      RESISTOR0603-RES        0603-RES          SparkFun    1
R2       22                      RESISTOR0603-RES        0603-RES          SparkFun    1
R3       1M                      RESISTOR0603-RES        0603-RES          SparkFun    1
R4       10k                     RESISTOR0603-RES        0603-RES          SparkFun    1
R5       330                     RESISTOR0603-RES        0603-RES          SparkFun    1
R6       330                     RESISTOR0603-RES        0603-RES          SparkFun    1
R7       330                     RESISTOR0603-RES        0603-RES          SparkFun    1
R8       330                     RESISTOR0603-RES        0603-RES          SparkFun    1
R9       1K                      RESISTOR0603-RES        0603-RES          SparkFun    1
R10      10K                     RESISTOR0603-RES        0603-RES          SparkFun    1
R11      2k                      RESISTOR0603-RES        0603-RES          SparkFun    1
R13      2M                      RESISTOR0603-RES        0603-RES          SparkFun    1
R14      220k                    RESISTOR0603-RES        0603-RES          SparkFun    1
R15      330                     RESISTOR0603-RES        0603-RES          SparkFun    1
R16      220k                    RESISTOR0603-RES        0603-RES          SparkFun    1
R17      2M                      RESISTOR0603-RES        0603-RES          SparkFun    1
R18      1K                      RESISTOR0603-RES        0603-RES          SparkFun    1
R21      10K                     RESISTOR0603-RES        0603-RES          SparkFun    1
R22      10K                     RESISTOR0603-RES        0603-RES          SparkFun    1
R23      10K                     RESISTOR0603-RES        0603-RES          SparkFun    1
R24      330                     RESISTOR0603-RES        0603-RES          SparkFun    1
R25      10K                     RESISTOR0603-RES        0603-RES          SparkFun    1
RL       RELAYPTH3               RELAYPTH3               RELAY-T9A-2       SparkFun    1
S1                               SWITCH-SPST-SMD-A       SWITCH-SPST-SMD-A SparkFun    1
U$2      CRYSTAL_SMT             CRYSTAL_SMT             CRYSTAL-SMD-5X3   SparkFun    1
U1       TPS61200                TPS61200                QFN-10_PAD        SparkFun    1
U2       MCP73831                MCP73831                SOT23-5           SparkFun    1
VR33     3.3V                    V_REG_LDOSMD            SOT23-5           SparkFun    1
X1       USBSMD                  USBSMD                  USB-MINIB         SparkFun    1
